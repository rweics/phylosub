#include "Tssb.hpp"

#include <iomanip> 
using std::setfill;
using std::setw;

Tssb::Tssb(double dp_alpha, double dp_gamma, 
    Alleles* root_node, vector<Datum*> data,
     int min_depth, int max_depth, double alpha_decay,
      const char* tree_filename = "", const char* data_filename = "") {
    
    if (data_filename != "")
        load_data(data_filename, &data);

    if (tree_filename != "") {
        map<int, Alleles*> nodes;
        load_tree(tree_filename, &nodes, &data);
        root_node = nodes[0];
    }
    else {
        /* assign data points to nodes? */
    }

    this->min_depth = min_depth;
    this->max_depth = max_depth;
    this->dp_alpha = dp_alpha;
    this->dp_gamma = dp_gamma;
    this->alpha_decay = alpha_decay;
    this->root_node = root_node;
    this->min_dp_alpha = 1.0;
    this->max_dp_alpha = 50.0;
    this->min_dp_gamma = 1.0;
    this->max_dp_gamma = 10.0;
    this->min_alpha_decay = 0.05;
    this->max_alpha_decay = 0.80;
    this->data = data;
    root_node->tssb = this;

    for (int i = 0; i < data.size(); i++) {
        assignments.push_back(data[i]->node);
    }

}

Tssb::~Tssb() {
    vector<Alleles*> nodes;
    get_nodes_dfs(root_node, &nodes);
    for (int i = 0; i < nodes.size(); i++) {
        for (std::set<Datum*>::iterator it=nodes[i]->data.begin(); it!=nodes[i]->data.end(); it++)
                delete *it;
        delete nodes[i];
    }
}

void Tssb::resample_assignments() {
    srand(time(NULL));
    gsl_rng *r = gsl_rng_alloc(gsl_rng_mt19937);
    double epsilon = 2.22044604925031308e-16; // finfo(float64).eps
    vector<int> lengths;
    for (int i = 0; i < data.size(); i++) {
        vector<Alleles*> ancestors = assignments[i]->get_ancestors();
        Alleles* current = root_node;
        vector<int> indices;
        for (int j = 1; j < ancestors.size(); j++) {
            int index = 0;
            std::vector<Alleles*>::iterator it;
            for (it=current->children.begin(); it!=current->children.end(); it++) {
                if (*it == ancestors[j]) break;
                else index += 1;
            }
            current = *it;
            indices.push_back(index);
        }
        double max_u = 1.0;
        double min_u = 0.0;
        double llh_s = log(rand() / double(RAND_MAX)) + assignments[i]->logprob(data[i]);
        while (true) {
            double new_u = (max_u - min_u) * (rand() / double(RAND_MAX)) + min_u;
            pair<Alleles*, vector<int> > new_node_and_new_path = find_node(root_node, new_u, 0, r);
            Alleles* new_node = new_node_and_new_path.first;
            vector<int> new_path = new_node_and_new_path.second;
            double new_llh = new_node->logprob(data[i]);
            if (new_llh > llh_s) {
                if (new_node != assignments[i]) {
                    assignments[i]->remove_datum(data[i]);
                    new_node->add_datum(data[i]);
                    assignments[i] = new_node;
                }
                lengths.push_back(new_path.size());
                break;
            } else if ((max_u > min_u ? max_u - min_u : min_u - max_u) < epsilon) {
                cout << "Slice sampler shrank down.  Keep current state." << endl;
                break;
            } else {
                double path_comp = path_lt(indices, new_path);
                if (path_comp < 0) {
                    min_u = new_u;
                } else if (path_comp > 0) {
                    max_u = new_u;
                } else {
                    cout << "Slice sampler weirdness." << endl;
                    throw("Slice sampler weirdness.");
                }
            }
        }
    }
    gsl_rng_free(r);
}

double path_lt(vector<int> path1, vector<int> path2) {
    if (path1.size() == 0 && path2.size() == 0) {
        return 0.0;
    } else if (path1.size() == 0) {
        return 1.0;
    } else if (path2.size() == 0) {
        return -1.0;
    } else {
        string s1 = "";
        string s2 = "";
        for (int i = 0; i < path1.size(); i++) {
            std::ostringstream stm;
            stm << setfill('0') << setw(3);
            stm << path1[i];
            s1 += stm.str();
        }
        for (int i = 0; i < path2.size(); i++) {
            std::ostringstream stm;
            stm << setfill('0') << setw(3);
            stm << path2[i];
            s2 += stm.str();
        }
        return strcmp(s2.c_str(), s1.c_str());
    }
}

pair<Alleles*, vector<int> > Tssb::find_node(Alleles* root, double u, int depth, gsl_rng *r) {
    pair<Alleles*, vector<int> > result;
    if (depth >= max_depth || u < root->main) {
        result.first = root;
        return result;
    } else {
        u = (u - root->main) / (1.0 - root->main);
        while (root->children.empty() || 1.0 - prod(minus_vec(1.0, root->sticks)) < u) {
            root->sticks.push_back(depth!=0 ? boundbeta(r, 1.0, dp_gamma) : 0.999);
            Alleles* new_child = root->spawn();
            new_child->main = min_depth <= (depth+1) ? boundbeta(r, 1.0, (pow(alpha_decay, (depth + 1)) * dp_alpha)) : 0.0;
            root->add_child(new_child);
        }
        vector<double> edges = minus_vec(1.0, cumprod(minus_vec(1.0, root->sticks)));
        int index = 0;
        for (int i = 0; i < edges.size(); i++) if (u > edges[i]) index++;
        edges.insert(edges.begin(), 0.0);
        u = (u - edges[index]) / (edges[index + 1] - edges[index]);
        std::vector<Alleles*>::iterator it=root->children.begin();
        for (int i = 0; i < index; i++) it++;
        pair<Alleles*, vector<int> > root_and_path = find_node(*it, u, depth + 1, r);
        root_and_path.second.insert(root_and_path.second.begin(), index);      
        return root_and_path;
    }
}

void load_data(const char* fname, vector<Datum*> *data) {
    string line,token;
    ifstream dfile(fname);
    int ctr=0;
    int data_id = 0;
    getline(dfile,line); // ignore top line
    while (getline(dfile,line,'\n')){
        istringstream iss(line);
        ctr=0;
        string name;
        vector<int> a, d, delta_v;
        vector<double> mu_v;
        int delta_r;
        double mu_r;
        while(getline(iss,token,'\t')){

        switch(ctr) {
            case 0:
                name = token;
                break;
            case 1:
                getIntsFromStr(token, &a);
                break;
            case 2:
                getIntsFromStr(token, &d);
                break;
            case 3:
                mu_r = atof(token.c_str());
                break;
            case 4:
                delta_r = atoi(token.c_str());
                break;
            case 5:
                getDoublesFromStr(token, &mu_v);
                break;
            case 6:
                getIntsFromStr(token, &delta_v);
                break;
            }
            ctr++;
        }
        Datum* datum = new Datum(name, a, d, mu_r, mu_v, delta_r, delta_v);
        datum->id = data_id;
        data->push_back(datum);
        data_id++;
    }
    dfile.close();
}

void load_tree(const char* fname, map<int, Alleles*> *nodes, vector<Datum*> *data) {
    string line,token;
    map<int, vector<int> > structure;
    ifstream dfile(fname);
    int ctr=0;
    getline(dfile,line); // ignore top line
    while (getline(dfile,line,'\n')){
        istringstream iss(line);
        int index;
        ctr=0;
        while(getline(iss,token,'\t')){
            switch(ctr) {

            case 0:
                index = atoi(token.c_str());
                (*nodes)[index] = new Alleles();
                (*nodes)[index]->id = index;
                break;
            case 1:
                getDoublesFromStr(token, &((*nodes)[index]->params));
                break;
            case 2:
                getDoublesFromStr(token, &((*nodes)[index]->pi));
                break;
            case 3:
                getIntsFromStr(token, &(structure[index]));
                break;
            case 4:
            {
                vector<int> data_ids;
                getIntsFromStr(token, &data_ids);
                for (int i = 0; i < data_ids.size(); i++) {
                    if (data_ids[i] != -1) {
                        (*nodes)[index]->add_datum((*data)[data_ids[i]]);
                        (*data)[data_ids[i]]->node = (*nodes)[index];
                    }
                }
                break;
            }
            case 5:
                (*nodes)[index]->main = atof(token.c_str());
                break;
            case 6:
                getDoublesFromStr(token, &((*nodes)[index]->sticks));
                break;
            }
            ctr++;
        }
    }
    dfile.close();
    for (int i = 0; i < nodes->size(); i++) {
        for (int j = 0; j < structure[i].size(); j++) {
            int child_id = structure[i][j];
            if (child_id == -1) break;
            (*nodes)[i]->children.push_back((*nodes)[child_id]);
            (*nodes)[child_id]->parent = (*nodes)[i];
        }
    }
}

void get_nodes_dfs(Alleles* root, vector<Alleles*> *nodes) {
    for (std::vector<Alleles*>::iterator it=root->children.begin(); it!=root->children.end(); it++) {
        get_nodes_dfs(*it, nodes);
    }
    nodes->push_back(root);
}

void Tssb::write_tree(const char* fname) {  
    ofstream fout;
    vector<Alleles*> nodes;
    map<Alleles*, int> node_map;
    get_nodes_dfs(root_node, &nodes);
    reverse(nodes.begin(), nodes.end());
    for (int i = 0; i < nodes.size(); i++) node_map[nodes[i]] = i;
    fout.open(fname);
    fout << "node_id    node_params node_pi children_node_ids   data_ids    main    sticks" << endl;
    for (int i = 0; i < nodes.size(); i++){ 
        fout << i << '\t';

        if (nodes[i]->params.size() == 0) fout << "-1\t";
        else {
            for (int j = 0; j < nodes[i]->params.size()-1; j++)
                fout << nodes[i]->params[j] << ',';
            fout << nodes[i]->params[nodes[i]->params.size()-1] << '\t';
        }

        if (nodes[i]->pi.size() == 0) fout << "-1\t";
        else {
            for (int j = 0; j < nodes[i]->pi.size()-1; j++)
                fout << nodes[i]->pi[j] << ',';
            fout << nodes[i]->pi[nodes[i]->pi.size()-1] << '\t';
        }

        int children_count = 0;
        for (std::vector<Alleles*>::iterator it=nodes[i]->children.begin(); it != nodes[i]->children.end(); it++) {
            children_count++;
            if (children_count != nodes[i]->children.size())
                fout << node_map[*it] << ',';
            else fout << node_map[*it] << '\t';
        }
        if (nodes[i]->children.size() == 0) fout << "-1\t";

        int data_count = 0;
        for (std::set<Datum*>::iterator it=nodes[i]->data.begin(); it != nodes[i]->data.end(); it++) {
            data_count++;
            if (data_count != nodes[i]->data.size())
                fout << (*it)->id << ',';
            else fout << (*it)->id << '\t';
        }
        if (nodes[i]->data.size() == 0) fout << "-1\t";

        fout << nodes[i]->main << '\t';

        if (nodes[i]->sticks.size() == 0) fout << "-1\n";
        else {
            for (int j = 0; j < nodes[i]->sticks.size()-1; j++)
                fout << nodes[i]->sticks[j] << ',';
            fout << nodes[i]->sticks[nodes[i]->sticks.size()-1] << '\n';
        }
    }   
    fout.close();   
}