#include "util.hpp"
using namespace std;

double log_factorial(int n){
    return lgamma(n + 1);
}

double log_bin_coeff(int n, int k){
    return log_factorial(n) - log_factorial(k) - log_factorial(n - k);
}

double log_binomial_likelihood(int x, int n, double mu){
    return  x * log(mu) + (n - x) * log(1 - mu);
}

double log_beta(double a, double b){
    return lgamma(a) + lgamma(b) - lgamma(a + b);
}

double logsumexp(double x[], int nx){
    double maxes=x[0], sum=0.0;
    for (int i=1;i<nx;i++)
        if (x[i]>maxes)
            maxes=x[i];
    for (int i=0;i<nx;i++)
        sum+=exp(x[i]-maxes);       
    return log(sum) + maxes;
}

void dirichlet_sample(int size, double alpha[], double *x, gsl_rng *r){
    gsl_ran_dirichlet(r,size,alpha,x);
    for(int i=0;i<size;i++)
        x[i]=x[i]+0.0001;
    double sum=0.0; 
    for(int i=0;i<size;i++)
        sum+=x[i];
    for(int i=0;i<size;i++)
        x[i]/=sum;
}

void getDoublesFromStr(string s, vector<double> *v) {
    if (s.find("-1") == -1) {
        while(s.length() && s.find(',') != -1) {
            int pos = s.find(',');
            v->push_back(atof(s.substr(0,pos).c_str()));
            s = s.substr(pos+1,s.length()-pos-1);
        }
        v->push_back(atof(s.c_str()));
    }
}

void getIntsFromStr(string s, vector<int> *v) {
    if (s.find("-1") == -1) {
        while(s.length() && s.find(',') != -1) {
            int pos = s.find(',');
            v->push_back(atoi(s.substr(0,pos).c_str()));
            s = s.substr(pos+1,s.length()-pos-1);
        }
        v->push_back(atoi(s.c_str()));  
    }
}

double prod(vector<double> nums) {
    double result = 1.0;
    for (int i = 0; i < nums.size(); i++) {
        result *= nums[i];
    }
    return result;
}

vector<double> cumprod(vector<double> nums) {
    vector<double> result;
    if (nums.size() > 0) result.push_back(nums[0]);
    for (int i = 1; i < nums.size(); i++) {
        result.push_back(result[i-1]*nums[i]);
    }
    return result;
}

vector<double> minus_vec(double a, vector<double> b) {
    vector<double> result;
    for (int i = 0; i < b.size(); i++) {
        result.push_back(a-b[i]);
    }
    return result;
}

double boundbeta(gsl_rng *r, double a, double b) {
    return  (1.0 - 2.22044604925031308e-16) * (gsl_ran_beta(r, a, b) - 0.5) + 0.5;
}
