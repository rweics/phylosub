#ifndef ALLELES_H
#define ALLELES_H

#include <set>
using std::set;

#include <ctime>

#include "Datum.hpp"

class Tssb; // forward declaration

class Alleles {

public:
// data
    set<Datum*> data;
    Alleles* parent;
    int id;
    double init_mean;
    double min_conc;
    double max_conc;
    double conc;
    double main;
    vector<double> pi;
    vector<double> pi1;
    vector<double> params;
    vector<double> params1;
    vector<double> sticks;
    vector<Alleles*> children;
    Tssb *tssb;

// methods
    bool has_data();
    int num_data();
    int num_local_data() {return data.size();}
    void kill();
    void add_datum(Datum* item) {this->data.insert(item);}
    void remove_datum(Datum* item);
    void resample_params() {;} // not implemented in python
    void add_child(Alleles* child) {this->children.push_back(child);}
    void remove_child(Alleles* child);
    vector<Datum*> get_data();
    vector<Alleles*> get_ancestors();
    double get_conc();
    double logprob(Datum* x);
    double complete_logprob();
    double data_log_likelihood(); // complete_logprob() not implemented in python?
    double sample(int num_data); // not sure what this does?
    Alleles* spawn();
    // not sure what global_param(key) is for
};

#endif