#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>
#include <stdlib.h>

#include <iostream>
#include <math.h>

#include <string>
using std::string;

#include <vector>
using std::vector;

#include <algorithm>
using std::reverse;

#include <set>
using std::set;

#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>

using namespace std;

// stat/math functions
double log_factorial(int n);
double log_bin_coeff(int n, int k);
double log_binomial_likelihood(int x, int n, double mu);
double log_beta(double a, double b);
double logsumexp(double x[], int nx);
void dirichlet_sample(int size, double alpha[], double *x, gsl_rng *r);
void getDoublesFromStr(string s, vector<double> *v);
void getIntsFromStr(string s, vector<int> *v);
double boundbeta(gsl_rng *r, double a, double b);
double prod(vector<double> nums);
vector<double> cumprod(vector<double> nums);
vector<double> minus_vec(double a, vector<double> b);

#endif