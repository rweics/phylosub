#ifndef DATUM_H
#define DATUM_H

#include "util.hpp"

class Alleles;

class Datum {
public:
    // Constructor
    Datum(string name, vector<int> a, vector<int> d, double mu_r, 
        vector<double> mu_v, int delta_r, vector<int> delta_v);

    // Data
    string name;
    vector<int> a; // number of reference allele read counts on the variant locus
    vector<int> d; // total number of reads at the locus
    double mu_r; // fraction of expected reference allele sampling from reference population
    vector<double> mu_v; // fraction of expected reference allele sampling from variant population
    int delta_r; // pseudo-count for the Dirichlet prior on genotype probabilities
    vector<int> delta_v; // pseudo-count for the Dirichlet prior on genotype probabilities
    int id;
    Alleles* node;
    
    double log_pi_r; // this is just 0 for scalar. 
    vector<double> log_pi_v; //set_log_mix_wts()
    
    vector<double> log_bin_norm_const; //log_bin_coeff(d,a);    

    // Methods
    void set_log_mix_wts(vector<int> delta, vector<double> *wts);
    double log_ll(double phi, int tp);
    double log_ll_multi(vector<double> phi);
    double log_complete_ll(double phi, double mu_r, double mu_v, int tp);
    double log_complete_ll_multi(double phi, double mu_r, double mu_v);
};

#endif