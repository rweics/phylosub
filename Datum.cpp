#include "Datum.hpp"

Datum::Datum(string name, vector<int> a, vector<int> d, double mu_r, 
        vector<double> mu_v, int delta_r, vector<int> delta_v) {

    this->name = name;
    this->a = a;
    this->d = d;
    this->mu_r = mu_r;
    this->mu_v = mu_v;
    this->delta_r = delta_r;
    this->delta_v = delta_v;

    // set_log_mix_wts(delta_r, &log_pi_r); ??? do we need this?
    set_log_mix_wts(delta_v, &log_pi_v);

    for (int i = 0; i < a.size(); i++) {
        log_bin_norm_const.push_back(log_bin_coeff(d[i], a[i]));
    }
}

void Datum::set_log_mix_wts(vector<int> delta, vector<double> *wts) {
    int sd=0;
    int NDELTA = delta.size();
    for(int i=0;i<NDELTA;i++)
        sd+=delta[i];
    double log_den = lgamma(sd+1);
    
    for(int i=0;i<NDELTA;i++){
        double log_num = lgamma(delta[i]+1);
        for(int j=0;j<NDELTA;j++)
            if (i!=j)
                log_num += lgamma(delta[j]);            
        wts->push_back(log_num - log_den);
    }
}

double Datum::log_ll(double phi, int tp) {
    int NDELTA = delta_v.size();
    double ll[NDELTA];
    for(int i=0;i<NDELTA;i++){
        ll[i] = log_complete_ll(phi,mu_r,mu_v[i], tp) + log_pi_r + log_pi_v[i];
    }
    return logsumexp(ll,NDELTA);
}

double Datum::log_complete_ll(double phi, double mu_r, double mu_v, int tp) {       
    double mu = (1 - phi) * mu_r + phi * mu_v;  
    return log_binomial_likelihood(a[tp], d[tp], mu) + log_bin_norm_const[tp];  
}

double Datum::log_ll_multi(vector<double> phi) {
    double result = 0.0;
    for (int tp = 0; tp < phi.size(); tp++) {
        result += log_ll(phi[tp], tp);
    }
    return result;
}

double Datum::log_complete_ll_multi(double phi, double mu_r, double mu_v) {     
    double result = 0.0;
    for (int tp = 0; tp < a.size(); tp++) {
        result += log_complete_ll(phi, mu_r, mu_v, tp);
    }
    return result;
}
