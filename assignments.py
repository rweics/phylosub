import numpy
from alleles import *
from numpy import *
from subprocess import call

def update_tree(tree, filename):

    def build_tree(nodes_str, node_id, tssb, assignments):
        nstr = nodes_str[node_id]
        children_id = [] if nstr[3] == '-1' else nstr[3].split(',')
        root_node = alleles()
        root_node.params = array([float64(param) for param in nstr[1].split(',')])
        root_node.pi = array([float64(pi) for pi in nstr[2].split(',')])
        root_node.tssb = tssb
        root = { 'node' : root_node,
                 'main' : float64(nstr[5]),
                 'sticks' : empty((0,1)) if nstr[6] == '-1\n' else array([[float64(stick)] for stick in nstr[6].split(',')]),
                 'children' : [] }
        root['node'].data = set([] if nstr[4] == '-1' else [int(datum) for datum in nstr[4].split(',')])
        for datum in root['node'].data: assignments[datum] = root['node']
        for child in children_id:
            child_node = build_tree(nodes_str, child, tssb, assignments)
            child_node['node']._parent = root['node']
            root['children'].append(child_node)
            root['node'].add_child(child_node['node'])
        return root

    f = open(filename, 'r')
    nodes_str, assignments = dict(), dict()
    for line in f:
        line_str = line.split('\t')
        nodes_str[line_str[0]] =line_str
    tree.root = build_tree(nodes_str, '0', tree, assignments)
    tree.assignments = [assignments[i] for i in range(len(assignments))]

def write_tree(tree, filename):

    def get_nodes(root):
        result = [root]
        for child in root['children']:
            result = result + get_nodes(child)
        return result

    f = open(filename, 'w')
    text = 'node_id\tnode_params\tnode_pi\tchildren_node_ids\tdata_ids\tmain\tsticks\n'
    nodes = get_nodes(tree.root)
    for node in nodes:
        text += str(nodes.index(node))+'\t'
        for i, param in enumerate(node['node'].params):
            if i != len(node['node'].params)-1:
                text+= str(param)+','
            else: text+= str(param)
        text += '\t'
        for i, pi in enumerate(node['node'].pi):
            if i != len(node['node'].pi)-1:
                text+= str(pi)+','
            else: text+= str(pi)
        text += '\t'
        for i, child in enumerate(node['children']):
            if i != len(node['children'])-1:
                text+= str(nodes.index(child))+','
            else: text+= str(nodes.index(child))
        if len(node['children']) == 0: text += '-1'
        text += '\t'
        for i, datum in enumerate(node['node'].data):
            if i != len(node['node'].data)-1:
                text+= str(datum)+','
            else: text+= str(datum)
        if len(node['node'].data) == 0: text += '-1'
        text += '\t'+str(node['main'])+'\t'
        for i, stick in enumerate(node['sticks']):
            if i != len(node['sticks'])-1:
                text+= str(stick[0])+','
            else: text+= str(stick[0])
        if len(node['sticks']) == 0: text += '-1'
        text += '\n'
    f.write(text)
    f.close()

def resample(tree, data_file):
    write_tree(tree, 'tree_to_resample.txt')
    call(['./resample.o', data_file, str(tree.dp_alpha), str(tree.dp_gamma), str(tree.alpha_decay)]) # call C++ output 'resample_assignments'
    update_tree(tree, 'tree_resampled.txt')