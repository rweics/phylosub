#ifndef TSSB_H
#define TSSB_H

#include "Alleles.hpp"

#include <map>
using std::map;

#include <fstream>
using std::ifstream;
using std::ofstream;

#include <sstream>
using std::istringstream;

class Tssb
{
public:
    Tssb(double dp_alpha, double dp_gamma, 
        Alleles* root_node, vector<Datum*> data, 
        int min_depth, int max_depth, double alpha_decay, 
        const char* tree_filename, const char* data_filename);

    pair<Alleles*, vector<int> > find_node(Alleles* root, double u, int depth, gsl_rng *r);
    void resample_assignments();
    ~Tssb();

    void write_tree(const char* fname);

    /* data */
    int min_depth;
    int max_depth;
    double dp_alpha;
    double dp_gamma;
    double alpha_decay;
    double min_dp_alpha;
    double max_dp_alpha;
    double min_dp_gamma;
    double max_dp_gamma;
    double min_alpha_decay;
    double max_alpha_decay;
    Alleles* root_node;
    vector<Datum*> data;
    vector<Alleles*> assignments;
};

void load_data(const char* fname, vector<Datum*> *data);
void load_tree(const char* fname, map<int, Alleles*> *nodes, vector<Datum*> *data);
void get_nodes_dfs(Alleles* root, vector<Alleles*> *nodes);
double path_lt(vector<int> path1, vector<int> path2);

#endif