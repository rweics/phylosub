Project info url:
http://www.google-melange.com/gsoc/project/details/google/gsoc2014/rweics/5639274879778816

Porting python code of [phylosub](https://github.com/morrislab/phylosub/tree/master/python) based on the following papers:

Inferring clonal evolution of tumors from single nucleotide somatic mutations
http://www.biomedcentral.com/content/pdf/1471-2105-15-35.pdf

Tree-Structured Stick Breaking for Hierarchical Data
http://www.cs.berkeley.edu/~jordan/papers/adams-ghahramani-jordan-nips11.pdf