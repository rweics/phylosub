#include "Alleles.hpp"

#include <map>
using std::map;

#include <fstream>
using std::ifstream;
using std::ofstream;

#include <sstream>
using std::istringstream;

/* prototypes */
double mh_loop(Alleles* root, int iters, double std, int burnin);
void update_params(Alleles* root, int tp);
void get_pi(vector<double> *pi, Alleles* root, int tp, bool isNew);
double param_posterior(Alleles* root, int tp, bool isNew);
void sample_cons_params(Alleles* root, int tp, gsl_rng *rand, double mh_std);
void get_nodes_dfs(Alleles* root, vector<Alleles*> *nodes);
void load_data(const char* fname, vector<Datum*> *data);
void load_tree(const char* fname, map<int, Alleles*> *nodes, vector<Datum*> *data);
void write_params(const char* fname, map<int, Alleles*> *nodes);

int main() {
    map<int, Alleles*> nodes;
    vector<Datum*> data;
    load_data("data.txt", &data);

    /* for testing */
    // for (int i = 0; i < data.size(); i++) {
    //  cout << data[i]->name << ' ';
    //  for (int j = 0; j < data[i]->log_bin_norm_const.size(); j++) {
    //      cout << data[i]->log_bin_norm_const[j] << ',';
    //  }
    //  cout << endl;
    // }44099937

    load_tree("c_tree.txt", &nodes, &data);

    /* for testing */
    // for (int i = 0; i < nodes.size(); i++) {
    //  cout << nodes[i]->has_data() << ' ' << nodes[i]->num_data() << ' ' << nodes[i]->num_local_data() << endl;
    // }
    // vector<Alleles*> nodes_dfs;
    // get_nodes_dfs(nodes[0], &nodes_dfs);
    // for (int i = 0; i < nodes_dfs.size(); i++)
    //  cout << nodes_dfs[i]->id << ", ";

    write_params("c_params.txt", &nodes);

    /* prevent memory leak */
    for (int i = 0; i < nodes.size(); i++) {
        delete nodes[i];
    }
    for (int i = 0; i < data.size(); i++) {
        delete data[i];
    }
    return 0;
}

double mh_loop(Alleles* root, int iters = 1000, double std = 0.01, int burnin = 0) {
    srand(time(NULL));
    gsl_rng *rand_num = gsl_rng_alloc(gsl_rng_mt19937);
    // vector<vector<double> > pi;
    for (int tp = 0; tp < root->pi.size(); tp++) {
        sample_cons_params(root, tp, rand_num, std);
        update_params(root, tp);
        // vector<double> pi_tp;
        // get_pi(&pi_tp, root, tp, false);
        // pi.push_back(pi_tp);
    }
    int ctr = 0;
    // vector<vector<double> > pi_new;
    for (int i = -1 * burnin; i < iters; i++) {
        int cf = 0;
        for (int tp = 0; tp < root->pi.size(); tp++) {
            sample_cons_params(root, tp, rand_num, std);
        }
        double a;// a=multi_param_posterior(tssb,1,ntps)-multi_param_posterior(tssb,0,ntps) + multi_correction_term(pi,pi_new,std[0],ntps) - multi_correction_term(pi_new,pi,std[0],ntps)
        if (log(rand() / RAND_MAX) < a) {
            for (int tp = 0; tp < root->pi.size(); tp++) {
                update_params(root, tp);
            }
            ctr += 1;
        }
    }
    return ctr * 1.0 / iters;
}

void update_params(Alleles* root, int tp) {
    for (std::set<Alleles*>::iterator it=root->children.begin(); it!=root->children.end(); it++) {
        update_params(*it, tp);
    }
    root->params[tp] = root->params1[tp];
    root->pi[tp] = root->pi1[tp];
}

void get_pi(vector<double> *pi, Alleles* root, int tp, bool isNew = false) {
    for (std::set<Alleles*>::iterator it=root->children.begin(); it!=root->children.end(); it++) {
        get_pi(pi, *it, tp);
    }
    if (isNew) {
        pi->push_back(root->pi1[tp]);
    } else {
        pi->push_back(root->pi[tp]);
    }
}

double param_posterior(Alleles* root, int tp, bool isNew) {
    double llh = 0.0;
    double p;
    vector<Datum*> data = root->get_data();
    if (isNew) {
        p = root->params1[tp];
    } else {
        p = root->params[tp];
    }
    for (int i = 0; i < data.size(); i++) {
        llh += data[i]->log_ll(p, tp);
    }
    for (std::set<Alleles*>::iterator it=root->children.begin(); it!=root->children.end(); it++) {
        llh += param_posterior(*it, tp, isNew);
    }
    return llh;
}

/* no stick breaking, updates pi with small perturbations */
void sample_cons_params(Alleles* root, int tp, gsl_rng *rand, double mh_std = 0.01) {
    vector<double> pi;
    vector<Alleles*> nodes;
    get_nodes_dfs(root, &nodes);
    get_pi(&pi, root, tp);
    double *alpha = new double[pi.size()];
    double *pi_new = new double[pi.size()];
    for (int i = 0; i < pi.size(); i++)
        alpha[i] = mh_std * pi[i] + 1;
    dirichlet_sample(pi.size(), alpha, pi_new, rand);
    for (int i = 0; i < pi.size(); i++)
        nodes[i]->pi1[tp] = pi_new[i];
    for (int i = 0; i < nodes.size(); i++) {
        nodes[i]->params1[tp] = pi_new[i];
        if (!nodes[i]->children.empty()) {
            for (std::set<Alleles*>::iterator it=nodes[i]->children.begin(); it!=nodes[i]->children.end(); it++) {
                nodes[i]->params1[tp] += (*it)->params1[tp];
            }   
        }
    }
    delete [] alpha;
    delete [] pi_new;
}

void get_nodes_dfs(Alleles* root, vector<Alleles*> *nodes) {
    for (std::set<Alleles*>::iterator it=root->children.begin(); it!=root->children.end(); it++) {
        get_nodes_dfs(*it, nodes);
    }
    nodes->push_back(root);
}

void load_data(const char* fname, vector<Datum*> *data) {
    string line,token;
    ifstream dfile(fname);
    int ctr=0;
    getline(dfile,line); // ignore top line
    while (getline(dfile,line,'\n')){
        istringstream iss(line);
        ctr=0;
        string name;
        vector<int> a, d, delta_v;
        vector<double> mu_v;
        int delta_r;
        double mu_r;
        while(getline(iss,token,'\t')){

        switch(ctr) {
            case 0:
                name = token;
                break;
            case 1:
                getIntsFromStr(token, &a);
                break;
            case 2:
                getIntsFromStr(token, &d);
                break;
            case 3:
                mu_r = atof(token.c_str());
                break;
            case 4:
                delta_r = atoi(token.c_str());
                break;
            case 5:
                getDoublesFromStr(token, &mu_v);
                break;
            case 6:
                getIntsFromStr(token, &delta_v);
                break;
            }
            ctr++;
        }
        Datum* datum = new Datum(name, a, d, mu_r, mu_v, delta_r, delta_v);
        data->push_back(datum);
    }
    dfile.close();
}

void load_tree(const char* fname, map<int, Alleles*> *nodes, vector<Datum*> *data) {
    string line,token;
    map<int, vector<int> > structure;
    ifstream dfile(fname);
    int ctr=0;
    getline(dfile,line); // ignore top line
    while (getline(dfile,line,'\n')){
        istringstream iss(line);
        int index;
        ctr=0;
        while(getline(iss,token,'\t')){
            switch(ctr) {

            case 0:
                index = atoi(token.c_str());
                (*nodes)[index] = new Alleles();
                (*nodes)[index]->id = index;
                break;
            case 1:
                getDoublesFromStr(token, &((*nodes)[index]->params));
                break;
            case 2:
                getDoublesFromStr(token, &((*nodes)[index]->pi));
                break;
            case 3:
                getIntsFromStr(token, &(structure[index]));
                break;
            case 4:
                vector<int> data_ids;
                getIntsFromStr(token, &data_ids);
                for (int i = 0; i < data_ids.size(); i++) {
                    if (data_ids[i] != -1) {
                        (*nodes)[index]->add_datum((*data)[data_ids[i]]);
                        (*data)[data_ids[i]]->node = (*nodes)[index];
                    }
                }
                break;
            }
            ctr++;
        }
    }
    dfile.close();
    for (int i = 0; i < nodes->size(); i++) {
        for (int j = 0; j < structure[i].size(); j++) {
            int child_id = structure[i][j];
            if (child_id == -1) break;
            (*nodes)[i]->children.insert((*nodes)[child_id]);
            (*nodes)[child_id]->parent = (*nodes)[i];
        }
    }
}

void write_params(const char* fname, map<int, Alleles*> *nodes){    
    ofstream fout;
    fout.open(fname);
    for (int i = 0; i < nodes->size(); i++){    
        fout << i << '\t';
        for (int j = 0; j < (*nodes)[i]->params.size()-1; j++)
            fout << (*nodes)[i]->params[j] << ',';
        fout << (*nodes)[i]->params[(*nodes)[i]->params.size()-1] << '\t';
        for (int j = 0; j < (*nodes)[i]->pi.size()-1; j++)
            fout << (*nodes)[i]->pi[j] << ',';
        fout << (*nodes)[i]->pi[(*nodes)[i]->pi.size()-1] << '\n';
    }   
    fout.close();   
}
