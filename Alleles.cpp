#include "Alleles.hpp"

void Alleles::kill() {
    if (this->parent) {
        this->parent->remove_child(this);
    }
    this->parent = NULL;
    this->children.clear(); // recursively remove the whole branch necessary?
}

bool Alleles::has_data() {
    if (!this->data.empty()) {
        return true;
    } else {
        for (std::vector<Alleles*>::iterator it=children.begin(); it!=children.end(); it++) {
            if ((*it)->has_data()) {
                return true;
            }
        }
    }
    return false;
}

int Alleles::num_data() {
    int sum = this->data.size();
    for (std::vector<Alleles*>::iterator it=children.begin(); it!=children.end(); it++) {
        sum += (*it)->num_data();
    }
    return sum;
}

void Alleles::remove_datum(Datum* item) {
    for (std::set<Datum*>::iterator it=data.begin(); it!=data.end(); it++) {
        if (*it == item) {
            data.erase(it); // maybe need to delete too, if dynamically allocated
        }
    }
}

double Alleles::get_conc() {
    if (!this->parent) {
        return this->conc;
    } else {
        return this->parent->get_conc();
    }
}

void Alleles::remove_child(Alleles* child) {
    for (std::vector<Alleles*>::iterator it=children.begin(); it!=children.end(); it++) {
        if (*it == child) {
            children.erase(it); // maybe need to delete too, if dynamically allocated
        }
    }
}

vector<Datum*> Alleles::get_data() {
    vector<Datum*> v;
    for (std::set<Datum*>::iterator it=data.begin(); it!=data.end(); it++) {
        v.push_back(*it);
    }
    return v;
}

vector<Alleles*> Alleles::get_ancestors() {
    vector<Alleles*> v;
    if (this->parent) {
        v = this->parent->get_ancestors();
    } 
    v.push_back(this);
    return v;
}

Alleles* Alleles::spawn() {
    srand(time(NULL));
    Alleles* node = new Alleles();
    node->parent = this;
    node->tssb = this->tssb;
    double rand_num = rand() / double(RAND_MAX);
    for (int i = 0; i < pi.size(); i ++) {
        node->pi.push_back(rand_num * pi[i]);
        pi[i] -= node->pi[i];
        node->params.push_back(node->pi[i]);
    }
    return node;
}

double Alleles::logprob(Datum* x) {
    return x->log_ll_multi(params);
}

double Alleles::complete_logprob() {
    // return sum([self.logprob([data]) for data in self.get_data()])
}