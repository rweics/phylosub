#include "Tssb.hpp"

#include <map>
using std::map;

#include <fstream>
using std::ifstream;
using std::ofstream;

#include <sstream>
using std::istringstream;

int main(int argc, char* argv[]) {
    vector<Datum*> data;
    double dp_alpha = atof(argv[2]);
    double dp_gamma = atof(argv[3]);
    double alpha_decay = atof(argv[4]);
    Tssb *tree = new Tssb(dp_alpha, dp_gamma, NULL, data, 0, 3, alpha_decay, "tree_to_resample.txt", argv[1]);
    tree->resample_assignments();
    tree->write_tree("tree_resampled.txt"); 
    return 0;
}